from PIL import Image
from io import BytesIO
import requests
import glob
import os

MAX_INDEX = 1085
COUNT_TO_LOAD = 100
PAGE_TO_LOAD = 1
REQUEST_URL = 'https://picsum.photos/v2/list'
picture_num_name = 1

def clean_dir():
	images = glob.glob('*.jpg')
	for image in images:
		os.remove(image)
def request_data():
	payload = {
		'page' : str(PAGE_TO_LOAD),
		'limit' : str(COUNT_TO_LOAD),
	}
	response = requests.get(REQUEST_URL, params=payload)
	return response.json()
def download(picture_URL):
	global picture_num_name
	response = requests.get(picture_URL)
	image = Image.open(BytesIO(response.content))
	image.save('{}.jpg'.format(picture_num_name))
	picture_num_name += 1
def main():
	try:
		clean_dir()
		picture_data = request_data()
		for picture in picture_data:
			print('Picture with id:{} by {}'.format(picture['id'], picture['author']), end='')
			download(picture['download_url'])
			print(' was successfully downloaded.')
	except Exception as e:
		print(e)
main()
input()