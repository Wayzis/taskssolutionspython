# Задача 10
# Напишите программу "Генератор персонажей" для игры. Пользователю должно быть предоставлено 30 пунктов, которые можно распределить между четырьмя характеристиками: Сила, Здоровье, Мудрость и Ловкость. Надо сделать так, чтобы пользователь мог не только брать эти пункты из общего "пула", но и возвращать их туда из характеристик, которым он решил присвоить другие значения.

# Костин Анатолий Константинович
# 22.10.2020

bank = 30
points = {
    'Сила': 0,
    'Здоровье': 0,
    'Мудрость': 0,
    'Ловкость': 0
}
alive = True

def info():
    for number, field in enumerate(points):
        print('{}. {}: {}'.format(number + 1, field, points[field]))
    print('Неиспользовано очков: {}'.format(bank))


while alive:
    info()
    while True:
        users_input = input('Введите название характеристики и количество очков: ').split()
        if not users_input:
            if bank:
                print('У Вас остались неиспользованные очки.')
            else:
                alive = False
            break
        if len(users_input) != 2:
            continue
        name = users_input[0].capitalize()
        value = int(users_input[1])

        if name in points and (bank >= value and value > 0 or value < 0 and points[name] >= -value):
            points[name] += value
            bank -= value
            break
print('\nРезультат:')
info()
input()
