# Задача 3. Вариант 31.
# Напишите программу, которая выводит имя "Эмиль Эрзог", и запрашивает его псевдоним. Программа должна сцеплять две эти строки и выводить полученную строку, разделяя имя и псевдоним с помощью тире.

# Костин Анатолий Константинович
# 17.09.2020

name = 'Эмиль Эрзог'
print('Герой нашей сегодняшней программы ' + name + '\nПод каким же именем мы знаем этого человека?')
expected = 'Андре Моруа'
while True:
	actual = input('Ваш ответ: ')
	if actual == expected:
		print('Всё верно: ' + name + ' - ' + expected)
		break
	print('Почти угадали! Попробуйте ещё раз.')
input('\n\nНажмите Enter для выхода.')
