# Задача 6. Вариант 31.
# Создайте игру, в которой компьютер загадывает название одной из восьми планет Солнечной системы, а игрок должен его угадать.

# Костин Анатолий Константинович
# 13.10.2020

array = ["Меркурий", "Венера", "Земля", "Марс", "Юпитер", "Сатурн", "Уран", "Нептун"]
print("Программа случайным образом загадывает название одной из восьми планет Солнечной системы, а игрок должен его угадать.")
import random
expected = random.choice(array)
while True:
	actual = input("Назовите название одной из восьми планет Солнечной системы: ")
	if expected == actual:
		print("Вы угадали!!!")
		break
	else:
		print("Вы не угадали!!!")
input('\n\nНажмите Enter для выхода.')
