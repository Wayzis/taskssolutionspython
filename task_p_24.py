# Практический проект 1 вариант 24
# Инструмент для обработки запросов в виде регулярных выражений. Программа позволяет пользователю вводить текстовую строку и затем в отдельном поле регулярное выражение. После запуска программа должна выдать все подходящие под регулярное выражение строчки или флаг ошибки.

# Костин Анатолий Константинович
# 7.11.2020

import re

def get_matches(text, expression):
	indexes = []
	accum = 0
	for i in re.findall(expression, text):
		indexes.append((accum + text.index(i), i))
		start = text.index(i) + len(i)
		accum += start
		text = text[start:]
	return indexes
def show_matches(matches):
	print('Результаты поиска:')
	if len(matches) == 0:
		print('Вхождений заданного выражения не нашлось.')
	else:
		print('Индекс:Вхождение')
		for match in matches:
			print(match[0], ':', match[1], sep='')
def main():
	try:
		text = input('Введите текстовую строку:')
		expression = input('Введите регулярное выражение:')
		matches = get_matches(text, expression)
		show_matches(matches)
	except Exception as e:
		print(e)
main()
input()
