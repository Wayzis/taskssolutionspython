﻿# Задача 8.
# Доработайте игру "Анаграммы" (см. М.Доусон Программируем на Python. Гл.4) так, чтобы к каждому слову полагалась подсказка. Игрок должен получать право на подсказку в том случае, если у него нет никаких предположений. Разработайте систему начисления очков, по которой бы игроки, отгадавшие слово без подсказки, получали больше тех, кто запросил подсказку.

# Костин Анатолий Константинович
# 15.10.2020

import random
word = random.choice(['python', 'answer', 'window', 'double'])

jumble = ''
word_copy = word
while word_copy:
    index = random.randint(0, len(word_copy)-1)
    jumble += word_copy[index]
    word_copy = word_copy[:index] + word_copy[index + 1:]
score = 10
prompt = False
print('Анаграмма: {}'.format(jumble))
print('Для использования подсказки введите "help".')
guess = input('Попробуйте угадать слово: ')
while guess.lower() != word.lower():
    if guess == 'help':
        if not prompt:
            guess = input('Подсказка использована. Первая половина слова: {}\nВаша версия: '.format(word[:len(word) // 2]))
            score //= 2
            prompt = True
        else:
            guess = input('Подсказка уже была использована.\nВаша версия: ')
    else:
        guess = input('Вы не угадали! Попробуйте снова: ')
        score -= 2
        if score < 0:
            score = 0
print('Вы угадали! Вам начислено очков: {}.'.format(score))
input('\n\nНажмите Enter для выхода.')
