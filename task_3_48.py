# Задача 3. Вариант 48.
# Напишите программу, которая выводит имя "Борис Николаевич Кампов", и запрашивает его псевдоним. Программа должна сцеплять две эти строки и выводить полученную строку, разделяя имя и псевдоним с помощью тире.

# Костин Анатолий Константинович
# 17.09.2020

name = 'Борис Николаевич Кампов'
print('Герой нашей сегодняшней программы ' + name + '\nПод каким же именем мы знаем этого человека?')
expected = 'Борис Полевой'
while True:
	actual = input('Ваш ответ: ')
	if actual == expected:
		print('Всё верно: ' + name + ' - ' + expected)
		break
	print('Почти угадали! Попробуйте ещё раз.')
input('\n\nНажмите Enter для выхода.')
