# Практический проект 3 вариант 57
# Программа для теггирования Mp3. Изменяет и добавляет ID3v1-теги в MP3-файлы. Можете попробовать добавить обложку альбома в хедер MP3-файла, или другие ID3v2-теги.

# Костин Анатолий Константинович
# 7.11.2020

from mutagen.id3 import ID3, TIT2, TALB, TCOM, COMM, APIC

# All tags: https://en.wikipedia.org/wiki/ID3

sup = [
	('название', 'title', 'tit2', lambda n: TIT2(encoding=3, text=n)),
	('альбом', 'album', 'talb', lambda n: TALB(encoding=3, text=n)),
	('композитор', 'composer', 'tcom', lambda n: TCOM(encoding=3, text=n)),
	('комментарий', 'comment', 'comm', lambda n: COMM(encoding=3, text=n)),
	('обложка', 'picture', 'apic', lambda n: APIC(mime='->', data=open(n, 'rb').read()))
]

def tagsInfo(tags):
	print('Текущие теги:')
	for k, v in tags.items():
		print('{}\n-\t{}'.format(k, v))
def main():
	audio_name = input('Введите имя музыкального файла MP3:')
	tags = ID3(audio_name)
	tagsInfo(tags)
	print('Поддерживаемые теги:')
	for t in sup:
		print('{}\t{}'.format(t[2], t[0]))
	while True:
		tag = input('Введите идентификатор тега для изменения:').lower()
		if tag == '':
			break
		for s in sup:
			if s[0] == tag or s[1] == tag or s[2] == tag:
				line = s
				break
		else:
			print('Тега "{}" не найдено.'.format(tag))
			continue
		val = input('Введите новое значение для тега "{}" ({}):'.format(line[0], line[1]))
		tags.add(line[3](val))
	tagsInfo(tags)
	confirm = input('Сохранить изменения? (y/N)')
	if confirm.lower() == 'y':
		tags.save()
try:
	main()
except BaseException as ex:
	print(ex)
input('Нажмите Enter для выхода')
