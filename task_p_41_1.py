# Практический проект 2 вариант 41
# Программа для массового создания эскизов для предварительного просмотра изображений. Обработка изображений может занимать достаточно долгое время. Особенно если изображение большого размера. Создайте программу обработки изображений, которая может взять сотню изображений и сконвертировать их до нужного размера в фоновом режиме. В качестве более сложного задания выделите одну ветку для изменения размера изображения, другую — для массового переименования эскизов и т.д.

# Костин Анатолий Константинович
# 7.11.2020

import glob
import os.path
from PIL import Image

PICTURES_PATH = 'pictures'
ORIGINALS_PATH = 'originals'
MINIATURES_PATH = 'miniatures'
THUMBNAIL_SIZE = (200, 200)

def make_miniature(source_path, destination_path):
	image = Image.open(source_path)
	image.thumbnail(THUMBNAIL_SIZE)
	image.save(destination_path)
def main():
	originals = os.path.join(PICTURES_PATH, ORIGINALS_PATH, '*.jpg')
	pics = glob.glob(originals)
	for source in pics:
		picture_name = os.path.basename(source)
		destination = os.path.join(PICTURES_PATH, MINIATURES_PATH, picture_name)
		print('Picture "{}"'.format(picture_name), end='')
		make_miniature(source, destination)
		print(' was successfully calculated.')
main()
input()
