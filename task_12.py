# Задача 12
# Разработайте игру "Крестики-нолики". (см. М.Доусон Программируем на Python гл. 6).

# Костин Анатолий Константинович
# 3.11.2020

import random

X = "X"
O = "0"
EMPTY = " "
TIE = "Ничья"
NUM_SQUARES = 9

def ask_yes_no(question, default = None):
	while True:
		response = input(question).lower()
		if response in ('y', 'n'):
			break
		else:
			if default != None:
				response = default
				break
	return response
def ask_number(question, low, high):
	response = None
	while response not in range(low, high):
		response = int(input(question))
	return response
def display_instruct():
	print(
'''Игра "Крестики-нолики".\n
Поле пронумеровано в соответствии со схемой:\n
\t\t0 | 1 | 2
\t\t--+---+--
\t\t3 | 4 | 5
\t\t--+---+--
\t\t6 | 7 | 8
\nУдачи!
''')
def pieces():
	go_first = ask_yes_no('Пользователь ходит первым? (Y/n):', 'y')
	if (go_first == 'y'):
		print('Теперь вы ходите крестиками.')
		human = X
		computer = O
	else:
		print('Теперь вы ходите ноликами.')
		human = O
		computer = X
	return human, computer
def new_board():
	board = []
	for square in range(NUM_SQUARES):
		board.append(EMPTY)
	return board
def display_board(board):
	print('\t\t', board[0], '|', board[1], '|', board[2], '\r\n\t\t --+---+--\r\n\t\t', board[3], '|', board[4], '|', board[5], '\r\n\t\t --+---+--\r\n\t\t', board[6], '|', board[7], '|', board[8])
def legal_moves(board):
	moves = []
	for square in range(NUM_SQUARES):
		if board[square] == EMPTY:
			moves.append(square)
	return moves
def winner(board):
	WAYS_TO_WIN = (
		(0, 1, 2),
		(3, 4, 5),
		(6, 7, 8),
		(0, 3, 6),
		(1, 4, 7),
		(2, 5, 8),
		(0, 4, 8),
		(2, 4, 6)
		)
	for row in WAYS_TO_WIN:
		if board[row[0]] == board[row[1]] == board[row[2]] != EMPTY:
			winner = board[row[0]]
			return winner
	if EMPTY not in board:
		return TIE
	return None
def human_move(board, human):
	legal = legal_moves(board)
	move = None
	while move not in legal:
		move = ask_number('Ход игрока. Выберете одно из полей (0 - 8): ', 0, NUM_SQUARES)
		if move not in legal:
			if (random.random() > 0.5):
				raise Exception()
			print('Ещё одна такая шутка и я вылечу.')
	print('Ход сделан.')
	return move
def computer_move(board, computer, human):
	board = board[:]
	BEST_MOVES = (4, 0, 2, 6, 8, 1, 3, 5, 7)
	print('Ход компьютера. Выбрано поле:', end=" ")
	for move in legal_moves(board):
		board[move] = computer
		if winner(board) == computer:
			print(move)
			return move
		board[move] = EMPTY
	for move in legal_moves(board):
		board[move] = human
		if winner(board) == human:
			print(move)
			return move
		board[move] = EMPTY
	for move in BEST_MOVES:
		if move in legal_moves(board):
			print(move)
			return move
def next_turn(turn):
	if turn == X:
		return O
	else:
		return X
def congrat_winner(the_winner, computer, human):
	if the_winner != TIE:
		print('Три', the_winner, 'в ряд!\n')
	else:
		print('Ничья!\n')
	if the_winner == computer:
		print('Поздравляю, ты проиграл.')
	elif the_winner == human:
		print('Поздравляю, ты победил!')
	elif the_winner == TIE:
		print('Ты не заслужил поздравлений, старайся лучше.')
def main():
	# Инструкции
	display_instruct()
	# Выбор первого игрока
	human, computer = pieces()
	turn = X
	# Создание доски
	board = new_board()
	# Отображение доски
	display_board(board)
	# Пока нет победителя и не ничья
	while not winner(board):
		# Ход пользователя
		if turn == human:
			# Ввод хода
			move = human_move(board, human)
			# Изменить доски
			board[move] = human
		# Ход компьютера
		else:
			# Рассчёт хода
			move = computer_move(board, computer, human)
			# Изменение доски
			board[move] = computer
		# Обновление доски
		display_board(board)
		# Переход хода
		turn = next_turn(turn)
	# Результат партии
	congrat_winner(winner(board), computer, human)
main()
input()
